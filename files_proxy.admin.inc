<?php

/**
 * @file
 * Admin page callbacks for the files proxy module.
 */

/**
 * Page generation function for admin/settings/files-proxy
 */
function files_proxy_admin_page() {
  $output = '';
  return $output . drupal_get_form('files_proxy_admin_settings_form');
}

/**
 * Form builder; Configure files proxy settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function files_proxy_admin_settings_form() {
  $form = array();
  $readme = drupal_get_path('module', 'files_proxy') . '/README.txt';

  $form['files_proxy_enabled'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Enable Files Proxy'),
    '#default_value'  => variable_get('files_proxy_enabled', FILES_PROXY_ENABLED),
  );
  $form['files_proxy_hotlink'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Redirect instead of downloading'),
    '#default_value'  => variable_get('files_proxy_hotlink', FILES_PROXY_HOTLINK),
    '#description'    => t('Useful on development servers. Do not enable on a production box.'),
  );
  $form['files_proxy_start_string'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Start of the files path, excluding the host name'),
    '#default_value'  => variable_get('files_proxy_start_string', FILES_PROXY_START_STRING),
    '#description'    => t('Current files path: %path', array('%path' => file_directory_path())),
  );
  $form['files_proxy_end_string'] = array(
    '#type'           => 'textfield',
    '#title'          => t('End of the files path, excluding the host name'),
    '#default_value'  => variable_get('files_proxy_end_string', FILES_PROXY_END_STRING),
    '#description'    => t('Current files path: %path', array('%path' => file_directory_path())),
  );

  $host_lookup = variable_get('files_proxy_host_lookup', array());
  $output = '';
  if (!empty($host_lookup)) {
    foreach ($host_lookup as $key => $value) {
      $output .= $key . ', ' . $value . "\n";
    }
    trim($output);
  }
  $form['files_proxy_host_lookup'] = array(
    '#type'           => 'textarea',
    '#title'          => t('Lookup table for your hosts'),
    '#default_value'  => $output,
    '#description'    => t('If your host name is not included in your files directory then you need to map it. One per line, use a comma to seperate it; "folder, hostname". Example: <br /> site1, example.com'),
  );

  $servers = variable_get('files_proxy_server_list', array());
  $output = '';
  if (!empty($servers)) {
    foreach ($servers as $value) {
      $output .= $value . "\n";
    }
    trim($output);
  }
  $form['files_proxy_server_list'] = array(
    '#type'           => 'textarea',
    '#title'          => t('List of hosts to try when local box does not have the file.'),
    '#default_value'  => $output,
    '#description'    => t('If the CDN module is installed, it will try the CDN domains if this is left blank. One per line "hostname". Example: <br /> example.com'),
  );
  $form['files_proxy_server_addr'] = array(
    '#type'           => 'textfield',
    '#title'          => t('IP Address to send local requests to'),
    '#default_value'  => variable_get('files_proxy_server_addr', FALSE),
    '#description'    => t('If left blank it will use the same server as the request.'),
  );

  return system_settings_form($form);
}

/**
 * Validate form values. Used to unset variables before they get saved.
 */
function files_proxy_admin_settings_form_validate($form, &$form_state) {
  global $conf;

  // Make sure files_proxy_start_string is not empty.
  if (empty($form_state['values']['files_proxy_start_string'])) {
    form_set_error('files_proxy_start_string', t('Can not be blank.'));
  }
  // Make sure files_proxy_end_string is not empty.
  if (empty($form_state['values']['files_proxy_end_string'])) {
    form_set_error('files_proxy_end_string', t('Can not be blank.'));
  }

  // Validate files_proxy_host_lookup
  if (!empty($form_state['values']['files_proxy_host_lookup'])) {
    $host_map = array();
    $lines = explode("\n", $form_state['values']['files_proxy_host_lookup']);
    if (empty($lines)) {
      form_set_error('files_proxy_host_lookup', t('Setting is not formatted correctly.'));
      return;
    }
    else {
      foreach ($lines as $line) {
        if (empty($line)) {
          continue;
        }
        $settings = explode(",", $line);
        $settings = array_map('trim', $settings);
        $settings = array_filter($settings);
        if (count($settings) != 2) {
          form_set_error('files_proxy_host_lookup', t('Setting is not formatted correctly.'));
          return;
        }
        $host_map[$settings[0]] = $settings[1];
      }
    }
    $form_state['values']['files_proxy_host_lookup'] = $host_map;
  }
  else {
    $form_state['values']['files_proxy_host_lookup'] = array();
  }

  // Validate files_proxy_server_list
  if (!empty($form_state['values']['files_proxy_server_list'])) {
    $lines = explode("\n", $form_state['values']['files_proxy_server_list']);
    if (empty($lines)) {
      form_set_error('files_proxy_server_list', t('Setting is not formatted correctly.'));
      return;
    }
    $lines = array_map('trim', $lines);
    $lines = array_filter($lines);
    $form_state['values']['files_proxy_server_list'] = $lines;
  }
  else {
    $form_state['values']['files_proxy_server_list'] = array();
  }

  // If the IP field is not blank, check that it is a valid address.
  if (!empty($form_state['values']['files_proxy_server_addr']) && ip2long($form_state['values']['files_proxy_server_addr']) === FALSE) {
    form_set_error('files_proxy_server_addr', t('Must be a valid IP address.'));
  }
}
